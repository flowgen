/*
 * Copyright (c) 2017 Mohamed Aslan <maslan@sce.carleton.ca>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <err.h>
#include <unistd.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

#include <netdb.h>      /* gethostbyname */
#include "client.h"
#include "../common.h"



/*
 * generates a single exponential random variable
 */
static double
rand_exp1(double lambda)
{
	double		u;

	u = (double)arc4random_uniform(RAND_MAX) / (RAND_MAX - 1);
	return - log (1 - u) / lambda;
} 

/*
 * generates n exponential random variables
 */
static void
rand_exp_n(int n, double *rvs, double lambda)
{
	int		i;

	if (n < 1)
		return;

	rvs[0] = 0;
	for (i = 1 ; i < n ; i++) {
		rvs[i] = rvs[i - 1] + rand_exp1(lambda);
		printf("r = %lf\n", rvs[i]);
	}
}

/*
 * generates a single pareto random variable
 */
static double
rand_pareto1(double shape, double scale)
{
	double		u;

	u = (double)arc4random_uniform(RAND_MAX) / (RAND_MAX - 1);
	return shape * pow(u, -1.0 / scale);
}

static void
handle_exp(int ev_queue, struct flow *f, struct log *lg)
{
	double			rv;
	ssize_t			bytes;

send:
	if (f->transport == EVT_TCP)
		bytes = write(f->skt, f->buf, f->payload_param.size);
	else
		bytes = sendto(f->skt, f->buf, f->payload_param.size, 0, (struct sockaddr *)&f->sa, (socklen_t)sizeof(f->sa));
	if (bytes == -1)
		warn("handle_exp");
	f_log(lg, "%lf, %d, %d, %zd\n", f->now, f->id, f->payload_param.size, bytes);
	fflush(lg->fd);
	/* next event */
	rv = rand_exp1(f->interarrival_param.mean);
	f->now += rv;
	if (f->now < f->to) {
		if (rv < TIMER_RES)
			goto send;
		atimer(ev_queue, f->id, (unsigned int)(rv * 1000));
	}
}

static void
handle_pareto(int ev_queue, struct flow *f, struct log *lg)
{
	double			rv;
	ssize_t			bytes;

send:
	if (f->transport == EVT_TCP)
		bytes = write(f->skt, f->buf, f->payload_param.size);
	else
		bytes = sendto(f->skt, f->buf, f->payload_param.size, 0, (struct sockaddr *)&f->sa, (socklen_t)sizeof(f->sa));
	if (bytes == -1)
		warn("handle_pareto");
	f_log(lg, "%lf, %d, %d, %zd\n", f->now, f->id, f->payload_param.size, bytes);
	fflush(lg->fd);
	/* next event */
	rv = rand_pareto1(f->interarrival_param.alphabeta.alpha, f->interarrival_param.alphabeta.beta);
	f->now += rv;
	if (f->now < f->to) {
		if (rv < TIMER_RES)
			goto send;
		atimer(ev_queue, f->id, (unsigned int)(rv * 1000));
	}
}

static int
flow_init(struct flow *f)
{
	struct hostent		*h;

	/* ip and port */
	memset(&f->sa, 0, sizeof f->sa);
	f->sa.sin_family = AF_INET;
	f->sa.sin_port = htons(f->dest_port);
	if ((h = gethostbyname(f->dest_ip))==0)
		err(1, "gethostbyname");
	memcpy(&f->sa.sin_addr.s_addr, h->h_addr, h->h_length);
	/* transport */
	if (f->transport == EVT_TCP) {
		if ((f->skt = socket(AF_INET, SOCK_STREAM, 0)) == -1)
			err(1, "socket");
		if ((connect(f->skt, (struct sockaddr *)&f->sa, (socklen_t)sizeof(f->sa))) == -1)
			err(1, "connect");
	} else if (f->transport == EVT_UDP) {
		if ((f->skt = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
			err(1, "socket");
	} else {
		errx(1, "invalid transport.");
	}
	/* payload */
	if (f->payload == EVP_CONSTANT)
		f->buf = (char *)malloc(f->payload_param.size);
	else
		f->buf = (char *)malloc(BUFSIZ);	/* FIXME */
	/* interarrival */
	if (f->interarrival == EVI_EXPONENTIAL)
		f->handler = handle_exp;
	else if (f->interarrival == EVI_PARETO)
		f->handler = handle_pareto;

	f->now = f->at;

	return 0;
}

static int
events_init(int ev_queue, struct flow *f, struct log *lg)
{
	switch (f->interarrival) {
	case EVI_EXPONENTIAL:
		if (f->at == 0)
			handle_exp(ev_queue, f, lg);
		else
			atimer(ev_queue, f->id, (unsigned int)(f->at * 1000));
		break;
	case EVI_PARETO:
		if (f->at == 0)
			handle_pareto(ev_queue, f, lg);
		else
			atimer(ev_queue, f->id, (unsigned int)(f->at * 1000));
		break;
	}
	return 0;
}

static void
print_info(struct flow *f)
{
	printf("INFO: flow %d.\n", f->id);
	printf("INFO: at %lf to %lf.\n", f->at, f->to);
	printf("INFO: destination IP: %s, port: %u.\n", f->dest_ip, f->dest_port);
	if (f->payload == EVP_CONSTANT)
		printf("INFO: payload size: %d bytes per packet.\n", f->payload_param.size);
	if (f->interarrival == EVI_EXPONENTIAL)
		printf("INFO: interarrival distribution: exponential, mean: %lf\n", f->interarrival_param.mean);
	else if (f->interarrival == EVI_PARETO)
		printf("INFO: interarrival distribution: pareto, shape: %lf, scale: %lf\n", f->interarrival_param.alphabeta.alpha, f->interarrival_param.alphabeta.beta);
	if (f->transport == EVT_UDP)
		printf("INFO: transport: UDP.\n");
	else if (f->transport == EVT_TCP)
		printf("INFO: transport: TCP.\n");
}

int
main(int argc, char **argv)
{
	char			*conf_file = NULL;
	int		 	 ch, i, n;
	int			 ev_queue;
	struct kevent		 kev;
	struct flow		 flows[MAX_FLOWS];
	struct log		 log;
	int			 n_flows;


	/* parse command line options */
	while ((ch = getopt(argc, argv, "f:")) != -1) {
		switch (ch) {
		case 'f':
			conf_file = strdup(optarg);
			break;
		default:
			errx(1, "invalid option.");
		}
	}
	argc -= optind;
	argv += optind;

	/* parse config file */
	if (!conf_file)
		conf_file = strdup(DEFAULT_CONFIG);
	if (parse_config(conf_file, flows, &n_flows, &log))
		errx(1, "error parsing config file.");

	/* log */
	log_init(&log);

#ifdef DEBUG
	for (i = 0 ; i < n_flows ; i++)
		print_info(&flows[i]);
#endif

	if (!n_flows)
		return 0;

	/* initialize flows */
	for (i = 0 ; i < n_flows ; i++)
		flow_init(&flows[i]);

	/* initialize event queue */
	if ((ev_queue = kqueue()) == -1)
		err(1, "kqueue");

	/* initialize events */
	for (i = 0 ; i < n_flows ; i++)
		events_init(ev_queue, &flows[i], &log);

	/* event loop */
	while (n_flows) {
		if ((n = kevent(ev_queue, NULL, 0, &kev, 1, NULL)) == -1)
			err(1, "kevent");
		if (kev.filter == EVFILT_TIMER) {
			i = (int)kev.ident;
			flows[i].handler(ev_queue, &flows[i], &log);
			if (flows[i].now >= flows[i].to) {
				--n_flows;
				close(flows[i].skt);
			}
		}
	}

	log_free(&log);

	return 0;
}
